def convertAsciiTXT(num_list):
    txt = []
    for i in num_list:
        txt.append(chr(i))
    return ''.join(txt)

def convertAsciiNUM(txt_list):
    num = []
    for i in txt_list:
        num.append(ord(i))
    return num

def convertNumList(inSTR):
    str_split = inSTR.split(',')
    num_list = [int(s) for s in str_split]
    return num_list

if __name__ == '__main__':
    # num_list = [97, 110, 115, 119, 101, 114, 95, 99, 111, 108]
    # txt_list = 'infos'
    # print(convertAsciiTXT(num_list))
    # print(convertAsciiNUM(txt_list))
    while True:
        inputSTR = input("아스키코드 변환 입력 1, 문자 변환 입력 2, 종료 3\n")

        if int(inputSTR) == 3:
            break
        elif int(inputSTR) == 1:
            inAscii = input("아스키코드로 변환 시 문자 입력\n")
            print(f'아스키코드 변환 결과: {convertAsciiNUM(inAscii)}')
            continue
        elif int(inputSTR) == 2:
            inSTR = input("문자로 변환 시 97,110,115 이런 식으로 입력\n")
            print(f'문자 변환 결과: {convertAsciiTXT(convertNumList(inSTR))}')
            continue