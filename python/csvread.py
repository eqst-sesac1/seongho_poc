import csv, base64, os

class MYCSV:
    def __init__(self, path, fileName) -> None:
        self.path = path
        self.fileName = fileName
        self.filePath = os.path.join(self.path, self.fileName)
        self.data = []

        # 필드 크기 제한을 최대 크기로 설정
        csv.field_size_limit(1024 * 1024 * 1000)

    # csv 읽어오는 메서드
    def read(self):
        self.data = []
        with open(self.filePath, newline="", encoding="utf-8", mode="r") as f:
            text = csv.reader(f)
            for row in text:
                self.data.append(row)
        return self.data
    
    def checkReqRes(self, i, d):
        if i == 0:
            dec = str(base64.b64decode(d[22]))
            n = 22
        elif i == 1:
            dec = str(base64.b64decode(d[23]))
            n = 23
        return dec, n

    def check(self, i, txt):
        a = 0
        for d in self.read():
            if a > 1:
                dec, n = self.checkReqRes(i, d)
                if txt in dec.lower():
                    print(d[:n], dec)
            a += 1

if __name__ == "__main__":
    # MYCSV 객체 생성 시 경로와 파일명 필요
    mycsv = MYCSV(r"C:\Users\K\Desktop\LGCNS", "240516 파트너센터.csv")
    # Burp Suite History CSV 파일의 23번('W열')이 요청값 24번('X열')이 응답값
    # 요청값을 보고 싶다면 0, 응답값을 보고 싶다면 1 그리고 검색할 문자열
    mycsv.check(0, "download")