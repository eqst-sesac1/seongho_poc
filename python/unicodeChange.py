import unicodedata

# 아직 미흡 업데이트 예정
class myunicode:
    def __init__(self, txt) -> None:
        self.txt = txt
        self.nfcuni = None
        self.nfkcuni = None
        self.nfduni = None
        self.nfkduni = None

    # NFC
    def nfcpoint(self):
        self.nfcuni = unicodedata.normalize('NFC', self.txt)
        codepoint = [f"u{ord(char):04X}" for char in self.nfcuni]
        return codepoint
    
    # NFKC
    def nfkcpoint(self):
        self.nfkcuni = unicodedata.normalize('NFKC', self.txt)
        codepoint = [f"u{ord(char):04X}" for char in self.nfkcuni]
        return codepoint

    # NFD
    def nfdpoint(self):
        self.nfduni = unicodedata.normalize('NFD', self.txt)
        codepoint = [f"u{ord(char):04X}" for char in self.nfduni]
        return codepoint
    
    # NFKD
    def nfkdpoint(self):
        self.nfkduni = unicodedata.normalize('NFKD', self.txt)
        codepoint = [f"u{ord(char):04X}" for char in self.nfkduni]
        return codepoint

myuni = myunicode("À")

# 정규화된 문자열 출력
print(f"NFC: {myuni.nfcpoint()}")
print(f"NFKC: {myuni.nfkcpoint()}")
print(f"NFD: {myuni.nfdpoint()}")
print(f"NFKD: {myuni.nfkdpoint()}")