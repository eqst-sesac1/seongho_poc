#!/bin/bash

# 이 쉘은 도메인과 이메일을 입력받아 HTTPS 인증서를
# 생성하고 만료 시 갱신까지 설정하는 쉘 스크립트
DOMAIN=$1
EMAIL=$2

# 입력 값 확인
if [ -z "$DOMAIN" ]; then
    echo "도메인을 입력해주세요."
    exit 1
fi

if ! [[ "$DOMAIN" =~ ^(([A-Za-z0-9]+\.)?[A-Za-z0-9]+\.[A-Za-z]{2,})$ ]]; then
    echo "도메인 : $1"
    echo "도메인을 정확히 입력해주세요."
    exit 1
fi

if [ -z "$EMAIL" ]; then
    echo "이메일을 입력해주세요."
    exit 1
fi

if ! [[ "$EMAIL" =~ ^[A-Za-z0-9]+@[A-Za-z0-9]+\.[A-Za-z]{2,}$ ]]; then
    echo "이메일 : $2"
    echo "이메일을 정확히 입력해주세요."
    exit 1
fi

# certbot 설치
echo -e "\nCertbot 설치 중\n"
sudo apt update
sudo apt install certbot -y

# certbot 인증서 발급
echo -e "\n인증서 발급 중\n"
sudo certbot certonly --standalone -d $DOMAIN --non-interactive --agree-tos --email $EMAIL

# 인증서 자동 갱신 설정
echo -e "\n인증서 자동 갱신 설정 중\n"
(crontab -l 2>/dev/null; echo "0 13 * * * certbot renew --quiet") | crontab -

echo -e "\n설정 완료"